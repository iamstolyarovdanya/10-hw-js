const ulTabs = document.querySelector(".tabs");
const liTabs = document.querySelectorAll(".tabs li");
const liContent = document.querySelectorAll(".tabs-content li");
liTabs.forEach((elem) => {
    elem.addEventListener("click", (event) => {
        liTabs.forEach((elem) => {
            elem.classList.remove("active")
            event.target.classList.add("active");
        });
        liContent.forEach(elem => elem.style.display = "none")
        const f = elem.dataset.f;
      const idF =  document.getElementById(f);
      idF.style.display = "block"
    })
})
